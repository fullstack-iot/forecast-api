# Forecast API 

![](https://img.shields.io/gitlab/pipeline-status/fullstack-iot/forecast-api?branch=00-system-setup&logo=gitlab&style=for-the-badge)
![](https://img.shields.io/gitlab/coverage/fullstack-iot/forecast-api/00-system-setup?logo=pytest&style=for-the-badge)
![](https://img.shields.io/badge/3.13.1-gray?style=for-the-badge&logo=python)
![](https://img.shields.io/badge/timescale-gray?style=for-the-badge&logo=timescale)
![](https://img.shields.io/badge/fastapi-gray?style=for-the-badge&logo=fastapi)
![](https://img.shields.io/badge/mlflow-gray?style=for-the-badge&logo=mlflow)

> This project attempts to predict energy demand using statistical models  and 
> grid scale storage supply using machine learning  models 

## Pre-requisites
- docker account

## Activity Diagram for Training
The following logic is valid for ensuring:
- Best performing model is always selected
- System never silently degrades below baseline
- Human intervention is triggered when needed
```plantuml
if (challenger > champion) then (yes)
 :deploy challenger;
 stop
else (no)
 if (baseline > champion) then (yes)
   :send grafana alert;
   :deploy baseline;
   stop
 else (no)
   :keep champion;
   stop
```

## Deployment Diagram
```plantuml
cloud ercot
rectangle train
database timeseries
rectangle model_store
rectangle serve
ercot - train: request data
train - timeseries: save data
train -d- model_store: store model
serve -l- model_store: load model

```
## Sequence Diagram
The CI versioning job should version the new model in mlflow and update the model store, so the serve service can load the new model
```plantuml
collections ercot 
participant process
database timeseries
participant train
participant grafana_prometheus
actor data_scientist
collections ci_versioning_job
database model_store
participant serve
actor api_user
== Daily Automated Training ==
process -> ercot: request daily data
process -> timeseries: load data
timeseries <- train: get daily data
train -> train: promote best model
train -> model_store: save best model

== Manual Data Scientist Retraining ==
train -> grafana_prometheus: send alert
grafana_prometheus -> data_scientist: receives w/ Grafana Notifier
data_scientist -> train: train new challenger
data_scientist -> ci_versioning_job: breaking change conventional commit
ci_versioning_job -> model_store: version new model
== Serve ==
api_user -> serve: request forecast
serve -> model_store: check for new model *🐢
model_store -> serve: load new model
serve -> api_user: predict forecast
```
>&ast;🐢 will setup webhook  when latency is high
