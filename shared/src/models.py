from datetime import datetime
from typing import Literal

from pydantic import BaseModel


class DataPoint(BaseModel):
    value: float
    timestamp: datetime


class SolarDatum(DataPoint):
    plant_limit: float
    short_term_forecast: float
    potential_production: float


type LogLevel = Literal["ERROR", "WARN", "INFO", "DEBUG"]
