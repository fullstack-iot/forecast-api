import logging
from datetime import timedelta, timezone

import pandas as pd

from .models import LogLevel, SolarDatum

CST = timezone(timedelta(hours=-6))


def convert_to_df(data: list[SolarDatum]) -> pd.DataFrame:
    df = pd.DataFrame([d.model_dump() for d in data])  # noqa: PD901
    df["timestamp"] = pd.to_datetime(df["timestamp"])
    return df


def setup_logger(level: LogLevel) -> None:
    # adds color
    for ind, lvl in enumerate(
        [logging.ERROR, logging.INFO, logging.WARNING, logging.DEBUG],
    ):
        logging.addLevelName(
            lvl,
            f"\033[0;3{ind + 1}m%s\033[1;0m" % logging.getLevelName(lvl),
        )
    # init logger
    logging.basicConfig(
        encoding="utf-8",
        format="%(levelname)s: %(message)s",
        level=level,
    )
