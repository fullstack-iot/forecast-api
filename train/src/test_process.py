from datetime import datetime

from shared.src.utils import CST

from .process import transform

CSV_DATA = """DELIVERY_DATE,HOUR_ENDING,ACTUAL_SYSTEM_WIDE,COP_HSL_SYSTEM_WIDE,STPPF_SYSTEM_WIDE,PVGRPP_SYSTEM_WIDE,DSTFlag
08/14/2024,19,11373.08,10460.4,10125.7,9450.7,N
08/14/2024,20,3266.92,2904.6,3296.7,3004.7,N
08/14/2024,21,143.2,73.2,58.5,52.9,N
08/14/2024,22,0.41,0.0,0.0,0.0,N
08/14/2024,23,0.46,0.0,0.0,0.0,N"""  # noqa: E501


def test_process() -> None:
    csv_bytes = CSV_DATA.encode()

    # Call the transform_data function
    solar_data = transform(csv_bytes)

    assert solar_data[0].timestamp == datetime(2024, 8, 14, 18, tzinfo=CST)
    assert solar_data[1].timestamp == datetime(2024, 8, 14, 19, tzinfo=CST)
