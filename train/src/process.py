import csv
from datetime import datetime

from shared.src.models import SolarDatum
from shared.src.utils import CST


def transform(csv_file: bytes) -> list[SolarDatum]:
    # Convert bytes to lines of strings that csv.DictReader can process
    csv_lines = csv_file.decode().splitlines()

    solar_data = []

    for row in csv.DictReader(csv_lines):
        # Parse date and hour ending
        date_str = row["DELIVERY_DATE"]
        hour_ending = int(row["HOUR_ENDING"])

        date = datetime.strptime(date_str, "%m/%d/%Y")  # noqa: DTZ007

        timestamp = date.replace(hour=hour_ending - 1, tzinfo=CST)

        # Create SolarDatum object
        datum = SolarDatum(
            timestamp=timestamp,
            value=float(row["ACTUAL_SYSTEM_WIDE"]),
            plant_limit=float(row["COP_HSL_SYSTEM_WIDE"]),
            short_term_forecast=float(row["STPPF_SYSTEM_WIDE"]),
            potential_production=float(row["PVGRPP_SYSTEM_WIDE"]),
        )

        solar_data.append(datum)

    return solar_data
