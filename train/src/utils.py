from os import environ

import yaml
from models import LogLevel
from pydantic import BaseModel


class Config(BaseModel):
    log_level: LogLevel = "DEBUG"


class ConfigMap(BaseModel):
    development: Config
    staging: Config


def load_config() -> Config:
    with open("train/cfg.yml", mode="rb") as file:
        contents = file.read()
        config_map = ConfigMap(**yaml.safe_load(contents))
        environment = environ.get("ENV", "development")
        match environment:
            case "staging":
                return config_map.staging
            case _:
                return config_map.development
