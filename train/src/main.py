import logging

from utils import load_config

from shared.models import SolarDatum
from shared.utils import setup_logger


def main() -> None:
    config = load_config("train")
    setup_logger(level=config.log_level)
    logging.info(f"{SolarDatum}")


if __name__ == "__main__":
    main()
