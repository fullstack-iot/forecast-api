from ipaddress import IPv4Address
from os import environ
from typing import Literal

import aiofiles
import yaml
from pydantic import BaseModel

from shared.src.models import LogLevel


class Config(BaseModel):
    uvicorn_reload: bool = False
    log_level: LogLevel = "DEBUG"
    host: IPv4Address
    environment: Literal["development", "beta"] = "development"


class ConfigMap(BaseModel):
    development: Config
    beta: Config


async def load_config() -> Config:
    async with aiofiles.open("cfg.yml", mode="rb") as file:
        contents = await file.read()
        config_map = ConfigMap(**yaml.safe_load(contents))
        environment = environ.get("ENV", "development")
        match environment:
            case "beta":
                return config_map.beta
            case _:
                return config_map.development
