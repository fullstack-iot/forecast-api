from datetime import datetime
from typing import Annotated

import numpy as np
from fastapi import Depends, FastAPI, Response
from numpy.typing import NDArray
from pydantic import AfterValidator, BaseModel, Field
from sklearn.linear_model import LinearRegression

from shared.src.models import DataPoint


def is_future(dt: datetime) -> datetime:
    if dt.tzinfo is None:
        raise ValueError("Timestamp must have timezone information")
    now = datetime.now(tz=dt.tzinfo)
    if dt < now:
        raise ValueError("Timestamp must be in the future")
    return dt


class TimeRange(BaseModel):
    start: Annotated[
        datetime,
        AfterValidator(is_future),
        Field(description="Future start time (must be after the current time)"),
    ]
    stop: Annotated[
        datetime,
        AfterValidator(is_future),
        Field(description="Future stop time (must be after the stop time)"),
    ]


app = FastAPI()

NpIntArray = NDArray[np.int64]


def timerange_to_np_unix(timerange: TimeRange) -> NpIntArray:
    return np.arange(
        timerange.start.timestamp(),
        timerange.stop.timestamp(),
        3600,
        dtype=np.int64,
    )


def prediction_service(
    timerange: TimeRange,
    model: LinearRegression,
) -> list[DataPoint]:
    unix_timestamps = timerange_to_np_unix(timerange).reshape(-1, 1)
    predicted_values = model.predict(unix_timestamps)
    tz = timerange.start.tzinfo
    iso_timestamps = [
        datetime.fromtimestamp(int(ts), tz=tz) for ts in unix_timestamps.flatten()
    ]
    return [
        DataPoint(timestamp=ts, value=pred)
        for ts, pred in zip(iso_timestamps, predicted_values)
    ]


def get_model() -> LinearRegression:
    # check if timestamps are in the future

    timestamps = np.array([1700000000, 1700001000, 1700002000, 1700003000, 1700004000])
    values = np.array([1.5, 2.1, 2.8, 3.2, 3.9])

    X = timestamps.reshape(-1, 1)  # noqa: N806
    y = values

    # Train and predict
    model = LinearRegression()
    model.fit(X, y)
    return model


@app.post("/predict")
def predict_controller(
    timerange: TimeRange,
    model: Annotated[LinearRegression, Depends(get_model)],
) -> list[DataPoint]:
    return prediction_service(timerange, model)


@app.get("/")
def health_check() -> Response:
    return Response("Running 💨", media_type="text/plain")
