import asyncio
import logging

import uvicorn

from shared.src.utils import setup_logger

from .utils import load_config


async def main() -> None:
    config = await load_config()
    setup_logger(config.log_level)
    logging.info(f"Running  with Config:  {config}")
    if __name__ == "__main__":
        uvicorn.run(
            "src.controller:app",
            host=str(config.host),
            reload=config.uvicorn_reload,
            log_level=config.log_level.lower(),
        )


asyncio.run(main())
